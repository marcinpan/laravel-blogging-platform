@extends('main')

@section('title', '| About')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-4">
            <h1>About Joggin' Bloggin'</h1>
            <p>
                It is a small blogging platform for people that are always on the move..
            </p>
        </div>
    </div>
@endsection
