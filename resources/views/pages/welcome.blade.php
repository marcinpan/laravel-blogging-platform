@extends('main')

@section('title', '| Home')

@section('content')
    @if (!Auth::check())
    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <h1>Welcome to JB Blog Platform!</h1>
                <p> Hello Stranger! Do you want to join in?</p>
                <p><a class="btn btn-primary btn-lg" href="{{ route('register') }}"> Click! </a></p>
            </div>
        </div>
    </div><!-- end of .row -->
    @endif

    <div class="row">
        <div class="col-md-7">
            <h1>Latest Posts:</h1><br><br>
            @foreach($posts as $post)
                <div class="post">
                    @if($post->image)
                        <a href="{{ action("BlogController@getSingle", ['slug'=> $post->slug])  }}"><img
                                    src="{{asset('images/' . $post->image)}}" class="img-responsive"></a>
                    @endif
                    <h3>
                        <a href="{{ action("BlogController@getSingle", ['slug'=> $post->slug])  }}"> {{$post->title}}</a>
                    </h3>

                    <p>Author: <a href="{{route("user.show",  $post->user_id)}}">{{$post->getUserName()}}</a></p>
                    <p>{{$post->created_at->diffForHumans()}}</p>
                    <p>
                        {{ substr(strip_tags($post->body), 0, 160) }}{!! strlen(strip_tags($post->body)) > 160 ? "..." : "" !!}
                    </p>
                    <a href="{{ action("BlogController@getSingle", ['slug'=> $post->slug])  }}">Read More</a>
                </div>
                <hr>
            @endforeach
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        {!! $posts->render() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-md-offset-1">
            <h1>Newest Blogs:</h1>
            @foreach($users as $user)
                <div class="row" style="padding-top: 30px">
                    <div class="list-group-item">
                        <h3><a href="{{route('user.show.posts', $user->id)}}">{{$user->name}}'s blog</a></h3>
                    </div>
                    <div class="list-group-item">
                        <h6>Created by <a href="{{route('user.show', $user->id)}}">{{$user->name}}</a>
                            at {{$user->created_at->format('M d, Y h:i') }}

                        </h6>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection