@extends('main')

@section('title', '| Blog')


@section('stylesheets')
    @if($user->theme == 'black')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/black.css')}}">
    @elseif($user->theme == 'darky')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/darky.min.css')}}">
    @elseif($user->theme == 'sandstone')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/sandstone.min.css')}}">
    @elseif($user->theme == 'united')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/united.min.css')}}">
    @endif
@endsection

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>
                {{$user->name}}'s blog:
            </h1>
            <hr>
        </div>
    </div>
    <div>

    @foreach ($posts as $post)
            <div class="post col-md-8 col-md-offset-2">
                @if($post->image)
                    <a href="{{ action("BlogController@getSingle", ['slug'=> $post->slug])  }}"><img
                                src="{{asset('images/' . $post->image)}}" class="img-responsive"></a>
                @endif
                <h3>
                    <a href="{{ action("BlogController@getSingle", ['slug'=> $post->slug])  }}"> {{$post->title}}</a>
                </h3>

                <p>Author: <a href="{{route("user.show",  $post->user_id)}}">{{$post->getUserName()}}</a></p>
                <p>{{$post->created_at->diffForHumans()}}</p>
                <p>
                    {{ substr(strip_tags($post->body), 0, 160) }}{{strlen(strip_tags($post->body)) > 160 ? "..." : ""}}
                </p>
                <a href="{{ action("BlogController@getSingle", ['slug'=> $post->slug])  }}">Read More</a>
                <hr>
            </div>
    @endforeach
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                {!! $posts->render() !!}
            </div>
        </div>
    </div>

@endsection
