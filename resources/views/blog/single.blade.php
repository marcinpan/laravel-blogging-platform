@extends('main')

<?php $titleTag = htmlspecialchars($post->title); ?>

@section('title', "| $titleTag")

@section('stylesheets')
    @if($post->users->theme == 'black')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/black.css')}}">
    @elseif($post->users->theme == 'darky')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/darky.min.css')}}">
    @elseif($post->users->theme == 'sandstone')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/sandstone.min.css')}}">
    @elseif($post->users->theme == 'united')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/united.min.css')}}">
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8">
            @if($post->image)
                <img src="{{asset('images/' . $post->image)}}" class="img-responsive">
            @endif
            <h1>{{ $post->title }}</h1>

            <p class="lead">{!! $post->body !!}</p>
            <hr>
            <div id="backend-comments" class="well">
                <div class="row">
                    <div class="col-md-12">
                        <h3>
                            Comments
                            <small>  {{ $post->comments()->count() }} total</small>
                        </h3>
                    </div>

                    <div id="comment-form" style="margin-top: 50px;">
                        @if(Auth::check())
                            <div class="col-md-12">
                                {{ Form::open(['route' => ['comments.store', $post->id]]) }}
                                {{ Form::textarea('body', null, ['class' => 'form-control', 'rows' => '2']) }}
                            </div>
                            <div class="com-md-12 pull-right" style="padding:15px">
                                {{ Form::submit('Add Comment', ['class' => 'btn btn-success']) }}
                                {{ Form::close() }}
                            </div>
                        @else
                            <div class="col-md-12">
                                <h4>Sign up or login to comment</h4>
                            </div>

                        @endif
                    </div>
                </div>
                <div class="row" style=" padding-top: 20px">
                    @foreach ($post->comments as $comment)
                        @if(!$comment->parent_id)
                            <div class="comment col-md-12" style="padding-bottom:20px">
                                <div class="list-group-item">
                                    <h4 class="card-title">
                                        <a href="{{route('user.show', $comment->user_id)}}">
                                            {{$comment->getUserName()}}
                                        </a>
                                    </h4>
                                    <h6 class="card-subtitle mb-2 text-muted">{{$comment->created_at->diffForHumans()}}</h6>
                                    <p class="card-text">{{ $comment->body }}</p>
                                    <a data-toggle="collapse" href="#collapse{{$comment->id}}"
                                       class="card-link">Reply</a>
                                    @if ( Auth::id() == $comment->user_id || Auth::id() == $post->user_id)
                                        {!!Form::open(['method' => 'DELETE', 'route' => ['comments.destroy', $comment->id, $post->slug], 'style' => 'display: inline;'])!!}
                                        {!!Form::submit('Delete', ['class' => 'btn-link'])!!}
                                        {!!Form::close() !!}
                                    @endif
                                    <div id="collapse{{$comment->id}}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            @if(Auth::check())
                                                <div class="col-md-12">
                                                    {{ Form::open(['route' => ['comments.answer', $post->id, $comment->id]]) }}
                                                    {{ Form::textarea('body', null, ['class' => 'form-control', 'rows' => '2']) }}
                                                </div>
                                                <div class="com-md-12 pull-right" style="padding:15px">
                                                    {{ Form::submit('Reply', ['class' => 'btn btn-success']) }}
                                                    {{ Form::close() }}
                                                </div>
                                            @else
                                                <div class="col-md-12">
                                                    <h4>Sign up or login to comment</h4>
                                                </div>

                                            @endif
                                        </div>
                                    </div>

                                </div>
                            </div>
                        @endif
                        @foreach ($comment->comments as $sub_comment)
                            <div class="comment col-md-11 col-md-offset-1" style="padding-bottom:20px">
                                <div class="list-group-item">
                                    <h4 class="card-title">
                                        <a href="{{route('user.show', $sub_comment->user_id)}}">
                                            {{$sub_comment->getUserName()}}
                                        </a>
                                    </h4>
                                    <h6 class="card-subtitle mb-2 text-muted">{{$sub_comment->created_at->diffForHumans() }}</h6>
                                    <p class="card-text">{{ $sub_comment->body }}</p>
                                    @if ( Auth::id() == $sub_comment->user_id || Auth::id() == $post->user_id)
                                        @if ( Auth::id() == $sub_comment->user_id || Auth::id() == $post->user_id)
                                            {!!Form::open(['method' => 'DELETE', 'route' => ['comments.destroy', $sub_comment->id, $post->slug]])!!}
                                            {!!Form::submit('Delete', ['class' => 'btn-link'])!!}
                                            {!!Form::close() !!}
                                        @endif
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endforeach
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="well">
                <dl class="dl-horizontal">
                    <label>Created By:</label>
                    <p><a href="{{route('user.show', $post->user_id)}}">{{ $post->getUserName() }} </a></p>
                </dl>

                <dl class="dl-horizontal">
                    <label>Created At:</label>
                    <p>{{ date('M j, Y H:i',strtotime($post->created_at)) }}</p>
                </dl>

                <dl class="dl-horizontal">
                    <label>Last updated:</label>
                    <p>{{ date('M j, Y H:i', strtotime($post->updated_at)) }}</p>
                </dl>
                <hr>
                @if(Auth::id() == $post->user_id)
                    <div class="row">
                        <div class="col-sm-6">
                            {!! Html::linkRoute('posts.edit', 'Edit', array($post->id),array('class' => 'btn btn-primary btn-block'))  !!}
                        </div>
                        <div class="col-sm-6">
                            {!! Form::open(['route' => ['posts.destroy', $post->id], 'method' => 'DELETE']) !!}

                            {!! Form::submit('Delete', ['class'=> 'btn btn-danger btn-block']) !!}

                            {!! Form::close() !!}
                        </div>
                    </div>﻿
                @endif

                <div class="row">
                    <div class="col-md-12">
                        <a href="{{route('user.show.posts', $post->user_id)}}"
                           class="btn btn-default btn-block btn-h1-spacing">Go to Blog</a>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection