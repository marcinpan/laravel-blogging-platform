@extends('main')

@section('title', '|Edit Blog Post')

@section('stylesheets')


    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'link code',
            menubar: false
        });
    </script>

    @if($post->users->theme == 'black')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/black.css')}}">
    @elseif($post->users->theme == 'darky')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/darky.min.css')}}">
    @elseif($post->users->theme == 'sandstone')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/sandstone.min.css')}}">
    @elseif($post->users->theme == 'united')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/united.min.css')}}">
    @endif

@endsection

@section('content')


    <div class="row">

        {!! Form::model($post, ['route' => ['posts.update', $post->id], 'method' => 'PUT', 'files' => true]) !!}
        <div class="col-md-8">
            @if($post->image)
                <img src="{{asset('images/' . $post->image)}}">
                <br>
            @endif

            {{ Form::label('title', 'Title') }}
            {{ Form::text('title', null, ["class" => 'form-control input-lg']) }}

            {{--{{ Form::label('slug', 'Slug:', ['class' => 'form-spacing-top']) }}--}}
            {{--{{ Form::text('slug', null, ['class' => 'form-control']) }}--}}
            {{ Form::label('featured_image', 'Upload Featured Image:') }}
            {{ Form::file('featured_image') }}
            {{ Form::label('body', 'Body') }}
            {{ Form::textarea('body', null, ["class" => 'form-control']) }}
        </div>


        <div class="col-md-4">
            <div class="well">


                <dl class="dl-horizontal">
                    <label>Created At:</label>
                    <p>{{ date('M j, Y H:i',strtotime($post->created_at)) }}</p>
                </dl>

                <dl class="dl-horizontal">
                    <label>Last updated:</label>
                    <p>{{ date('M j, Y H:i', strtotime($post->updated_at)) }}</p>
                </dl>
                <hr>

                <div class="row">
                    <div class="col-sm-6">
                        {!! Html::linkRoute('blog.single', 'Cancel', array($post->slug),array('class' => 'btn btn-danger btn-block'))  !!}
                    </div>
                    <div class="col-sm-6">
                        {{ Form::submit('Save Changes', ['class' => 'btn btn-success btn-block']) }}
                    </div>
                </div>﻿

            </div>
        </div>
        {!! Form::close() !!}
    </div> <!-- end of .row (form) -->

@stop
