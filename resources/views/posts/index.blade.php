@extends('main')

@section('title', '|All Posts')

@section('stylesheets')
    @if($posts[0]->users->theme == 'black')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/black.css')}}">
    @elseif($posts[0]->users->theme == 'darky')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/darky.min.css')}}">
    @elseif($posts[0]->users->theme == 'sandstone')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/sandstone.min.css')}}">
    @elseif($posts[0]->users->theme == 'united')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/united.min.css')}}">
    @endif
@endsection

@section('content')

    <div class="row">
        <div class="col-md-10">
            <h1>All Posts</h1>
        </div>

        <div class="col-md-2">
            <a href="{{ route('posts.create') }}" class="btn btn-lg btn-block btn-primary">Create New Post</a>
        </div>

        <div class="col-md-12">
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <th>Image</th>
                <th>Title</th>
                <th>Body</th>
                <th>Created At</th>
                <th></th>
                </thead>

                <tbody>

                @foreach ($posts as $post)
                    @if(Auth::user()->id == $post->user_id)
                        <tr>
                            <td>
                                @if($post->image)
                                    <a href="{{ action("BlogController@getSingle", ['slug'=> $post->slug])  }}"><img
                                                src="{{asset('images/' . $post->image)}}" width="200" height="120"></a>
                                @endif
                            </td>
                            <td>{{ $post->title }}</td>
                            <td> {{ substr(strip_tags($post->body), 0, 50) }} {{ strlen(strip_tags($post->body)) > 50 ? "..." : "" }}</td>
                            <td>{{ date('M j, Y', strtotime($post->created_at)) }}</td>
                            <td>
                                <a href="{{ route('blog.single', $post->slug) }}" class="btn btn-default">View</a>
                                <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-default">Edit</a>
                            </td>
                        </tr>
                    @endif
                @endforeach

                </tbody>
            </table>

            <div class="text-center">
                {!! $posts->links() !!}

            </div>
        </div>
    </div>

@stop