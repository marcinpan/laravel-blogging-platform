@extends('main')

@section('title', '| Create New Post')

@section('stylesheets')


    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'link code',
            menubar: false
        });
    </script>

    @if($user->theme == 'black')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/black.css')}}">
    @elseif($user->theme == 'darky')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/darky.min.css')}}">
    @elseif($user->theme == 'sandstone')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/sandstone.min.css')}}">
    @elseif($user->theme == 'united')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/united.min.css')}}">
    @endif

@endsection
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>Create New Post</h1>
            <br>
            {!! Form::open(['route' => 'posts.store', 'files' => true]) !!}
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title', 'Write Post Title', ['class' => 'form-control']) }}

            {{--{{ Form::label('slug', 'Slug:') }}--}}
            {{--{{ Form::text('slug', null, array('class' => 'form-control', 'required' => '', 'minlength' => '5', 'maxlength' => '255')) }}--}}
            {{ Form::label('featured_image', 'Upload Featured Image:') }}
            {{ Form::file('featured_image') }}
            {{ Form::label('body', 'Post:') }}
            {{ Form::textarea('body', 'Write Post Body', ['class' => 'form-control']) }}
            {{ Form::submit('Create', ['class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;']) }}
            {!! Form::close() !!}
        </div>
    </div>


@endsection