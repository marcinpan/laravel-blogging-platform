@extends('main')

@section('title', '|View User')
{{--{{ $user->name }}--}}
@section('stylesheets')
    @if($user->theme == 'black')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/black.css')}}">
    @elseif($user->theme == 'darky')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/darky.min.css')}}">
    @elseif($user->theme == 'sandstone')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/sandstone.min.css')}}">
    @elseif($user->theme == 'united')
        <link rel="stylesheet" type="text/css" href="{{asset('/css/united.min.css')}}">
    @endif
@endsection



@section('content')
    <div class="well">
        <div class="row">
            <div class="col-md-2 col-md-offset-1">
                <img src="{{asset('images/default.jpeg')}}" class="img-responsive img-circle">
            </div>
            <div class="col-md-3">
                <strong><h1>{{$user->name }}</h1></strong>
                <strong>Email: {{$user->email }}</strong>
                <p>Joined on {{$user->created_at->format('M d, Y h:i a') }}</p>
                <div class=" badge">Total Posts: {{$posts_count}}</div>
                <div class=" badge">Total Comments: {{$comments_count}}</div>
            </div>
            <div class="col-md-2 pull-right" style="margin-top: 120px">
                <a class="btn btn-primary" href="{{route('user.show.posts', $user->id)}}">
                    Show blog
                </a>

            </div>
            @if(Auth::id() == $user->id)
                <div class="col-md-4 pull-right" style="margin-top: 120px">
                    <div class="form-group">
                        {!! Form::model($user, ['route' => ['change.theme', $user->id], 'method' => 'POST']) !!}
                        {{ Form::label('body', 'Select blog theme:') }}
                        {{ Form::select('theme', array(
                            null => 'default',
                            'darky' => 'darky',
                            'sandstone' => 'sandstone',
                            'united' => 'united',

                        )) }}
                        {{ Form::submit('Update', ['class' => 'btn btn-success']) }}
                        {!! Form::close() !!}
                    </div>

                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Latest Posts </h3></div>
                <div class="panel-body">
                    @if(!empty($latest_posts[0]))
                        @foreach($latest_posts as $latest_post)
                            <div class="post">
                                @if($latest_post->image)
                                <div class="col-md-4">
                                        <a href="{{ action("BlogController@getSingle", ['slug'=> $latest_post->slug])  }}"><img
                                                    src="{{asset('images/' . $latest_post->image)}}"
                                                    class="img-responsive" style="margin-top: 25px"></a>

                                </div>
                                <div class="col-md-8">
                                    <h3>
                                        <a href="{{ action("BlogController@getSingle", ['slug'=> $latest_post->slug])  }}"> {{$latest_post->title}}</a>
                                    </h3>
                                    <p>{{$latest_post->created_at->format('M d, Y')}}</p>
                                    <p>
                                        {{ substr(strip_tags($latest_post->body), 0, 160) }}{{strlen($latest_post->body) > 160 ? "..." : ""}}
                                    </p>
                                    <a href="{{ action("BlogController@getSingle", ['slug'=> $latest_post->slug])  }}">Read
                                        More</a>
                                </div>
                                @else
                                    <div class="col-md-12">
                                        <h3>
                                            <a href="{{ action("BlogController@getSingle", ['slug'=> $latest_post->slug])  }}"> {{$latest_post->title}}</a>
                                        </h3>
                                        <p>{{$latest_post->created_at->format('M d, Y')}}</p>
                                        <p>
                                            {{ substr(strip_tags($latest_post->body), 0, 160) }}{{strlen($latest_post->body) > 160 ? "..." : ""}}
                                        </p>
                                        <a href="{{ action("BlogController@getSingle", ['slug'=> $latest_post->slug])  }}">Read
                                            More</a>
                                    </div>
                                @endif

                            </div>
                            <hr>
                        @endforeach
                    @else
                        <p>Has not written any post till now.</p>
                    @endif
                </div>
            </div>


        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Latest Comments</h3></div>
                <div class="list-group">
                    @if(!empty($latest_comments[0]))
                        @foreach($latest_comments as $latest_comment)
                            <div class="list-group-item">
                                <p>{{ $latest_comment->body }}</p>
                                <p>On {{ $latest_comment->created_at->format('M d, Y H:i a') }}</p>
                                <p>On post
                                    <a href="{{ route('blog.single', $latest_comment->posts->slug) }}">
                                        {{ $latest_comment->posts->title }}
                                    </a>
                                </p>
                            </div>
                        @endforeach
                    @else
                        <div class="list-group-item">
                            <p>Has not written any comments till now.</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>


@endsection
