<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function(){

    Route::get('blog', ['uses' =>'BlogController@getIndex', 'as' => 'blog.index']);

    Route::get('blog/{slug}', ['as' => 'blog.single', 'uses' =>'BlogController@getSingle'] )->where('slug', '[\w\d\-\_]+');

    Route::get('/about', 'PagesController@getAbout');

    Route::get('/', 'PagesController@getIndex');

    Route::resource('posts', 'PostController');

    Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

    Route::post('/register', 'Auth\RegisterController@register');

    Route::post('comments/{post_id}', ['uses' =>'CommentController@store', 'as' => 'comments.store']);

    Route::post('comments/{post_id}/{parent_id}', ['uses' =>'CommentController@answer', 'as' => 'comments.answer']);

    Route::delete('comments/destroy/{comment_id}/{post_id}', ['uses' => 'CommentController@destroy', 'as' => 'comments.destroy']);

    Route::post('/change/{user_id}', ['as' => 'change.theme', 'uses' => 'UserController@theme']);
 //   Route::resource('comments', 'CommentController', ['except' => ['create']]);
});

Auth::routes();

//user's profiless
Route::get('/user/{id}', ['as' => 'user.show', 'uses' => 'UserController@show']);

// display list of posts
Route::get('/user/{id}/posts', ['as' => 'user.show.posts', 'uses' => 'UserController@user_posts']);


//Route::get('/home', 'HomeController@index');
Route::get('/home', 'PostController@index');