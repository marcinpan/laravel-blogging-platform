<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //deleting existing tables
        DB::table('users')->delete();
        DB::table('posts')->delete();
        DB::table('comments')->delete();

        //create admin acount
        App\User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin123'),
            'theme' => null,
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);

        //creating users
        factory(App\User::class, 5)->create();

        //creating random posts
        factory(App\Post::class, 10)->create();

        //creating random comments
        factory(App\Comment::class, 30)->create();

        //creating replies to comments
        factory(App\Comment::class, 'reply', 20)->create();
    }


}
