<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    $themes = [null, 'darky', 'sandstone', 'united'];

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'theme' => $themes[array_rand($themes)],
        'remember_token' => str_random(10),
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s"),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Post::class, function (Faker\Generator $faker) {
    $title = $faker->sentence;
    $title = rtrim($title, ".");

    return [
        'user_id' => random_int(\DB::table('users')->min('id'), \DB::table('users')->max('id')),
        'title' => $title,
        'body' => $faker->text(500),
        'slug' => strtolower(str_replace(' ', '_', $title)) . '_' . $faker->unique()->randomDigit,
        'image' => rand(0, 5) == 0 ? null : rand(1, 11) . '.jpg',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s"),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Comment::class, function (Faker\Generator $faker) {
    return [
        'post_id' => random_int(\DB::table('posts')->min('id'), \DB::table('posts')->max('id')),
        'user_id' => random_int(\DB::table('users')->min('id'), \DB::table('users')->max('id')),
        'body' => $faker->sentence,
        'parent_id' => 0,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s"),
    ];
});

$factory->defineAs(App\Comment::class, 'reply', function ($faker) use ($factory) {
    $comment = DB::table('comments')->where('parent_id', '==', 0)->inRandomOrder()->first();
    return [
        'post_id' => $comment->post_id,
        'user_id' => random_int(\DB::table('users')->min('id'), \DB::table('users')->max('id')),
        'body' => $faker->sentence,
        'parent_id' => $comment->id,
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s"),
    ];
});