<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'post_id');
    }

    public function getUserName()
    {
        $user_id = $this->user_id;

        $user = User::findOrFail($user_id);

        return $user->name;
    }
}
