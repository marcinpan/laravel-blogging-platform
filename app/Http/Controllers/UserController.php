<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display the posts of a particular user
     *
     * @param int $id
     * @return Response
     */
    public function user_posts($id)
    {
        $posts = Post::where('user_id', $id)->orderBy('created_at', 'desc')->paginate(5);
        $user = User::findOrFail($id);
        return view('blog.index')->withPosts($posts)->withUser($user);
    }

    /**
     * profile for user
     */
    public function show(Request $request, $id)
    {
        $data['user'] = User::findOrFail($id);
        if (!$data['user'])
            return redirect('/');

        $data['comments_count'] = $data['user']->comments->count();
        $data['posts_count'] = $data['user']->posts->count();

        $data['latest_posts'] = $data['user']->posts->take(5);
        $data['latest_comments'] = $data['user']->comments->take(5);
        return view('user.show', $data);
    }

    public function theme(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->theme = $request->theme;
        $user->save();

        $data['user'] = User::findOrFail($id);
        if (!$data['user'])
            return redirect('/');

        $data['comments_count'] = $data['user']->comments->count();
        $data['posts_count'] = $data['user']->posts->count();

        $data['latest_posts'] = $data['user']->posts->take(5);
        $data['latest_comments'] = $data['user']->comments->take(5);
        return view('user.show', $data);
        return view('user.show');
    }

}
