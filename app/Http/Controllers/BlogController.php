<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Post;

class BlogController extends Controller
{
    public function getIndex() {
        $posts = Post::paginate(3);
        return view('blog.index')->withPosts($posts);
    }

    public function getSingle($slug) {
        //fetch from the database based on slug
        //return the view and pass in the post object
        $post = Post::where('slug', $slug)->first();
//        $comments = Comment::where('user_id', $post->user_id)->orderBy('created_at', 'desc');
        if (!$post)
            return redirect('/')->withErrors('requested page not found');

        return view('blog.single')->withPost($post);
    }


}
