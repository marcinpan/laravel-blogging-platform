<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CommentController extends Controller
{

    public function store(Request $request, $post_id)
    {
        $this->validate($request, array(
            'body' =>'required|min:5|max:500'
        ));

        $comment = new Comment();
        $comment->body = $request->body;
        $comment->posts()->associate($post_id);
        $comment->users()->associate(Auth::user()->id);
        $comment->parent_id = 0;

        $post = Post::findOrFail($post_id);

        $comment->save();

        Session::flash('success', 'Comment was added!');

        return redirect()->action("BlogController@getSingle", ['slug'=> $post->slug]);

    }

    public function answer(Request $request, $post_id, $parent_id)
    {
        $this->validate($request, array(
            'body' =>'required|min:5|max:500'
        ));

        $comment = new Comment();
        $comment->body = $request->body;
        $comment->posts()->associate($post_id);
        $comment->users()->associate(Auth::user()->id);
        $comment->parent_id = $parent_id ;
        $post = Post::findOrFail($post_id);

        $comment->save();

        Session::flash('success', 'Comment was added!');
        return redirect()->action("BlogController@getSingle", ['slug'=> $post->slug]);

    }

    public function destroy($id, $post_slug)
    {
        $comment = Comment::findOrFail($id);

        $sub_comments = Comment::where('parent_id', $id);
        $sub_comments->delete();
        $comment->delete();

        Session::flash('success', 'The post was succesfully deleted');
        return redirect()->action("BlogController@getSingle", ['slug' => $post_slug]);
    }
}
