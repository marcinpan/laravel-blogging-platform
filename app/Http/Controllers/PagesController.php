<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Session;


class PagesController extends Controller {

    public function getIndex(){
        $posts = Post::orderBy('created_at', 'desc')->paginate(4);
        $users = User::orderBy('id', 'desc')->limit(5)->get();
        return view('pages.welcome')->withPosts($posts)->withUsers($users);

    }

    public function getAbout()
    {
        return view('pages.about');
    }

    public function setSettings(Request $request)
    {
       /* $this->validate($request, array(
            'blog' => 'required|max:255'
        ));

        $users = Auth::user();
        $users->blog = $request->blog;

        $users->save();

        Session::flash('success', 'Your blog has been created');

        return view('pages.welcome');*/
    }

    public function updateSettings()
    {
        //
    }
}