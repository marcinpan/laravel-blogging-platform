<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Image;
use Storage;


//use Intervention\Image\Image;


class PostController extends Controller
{
    public function __construct() {
        $this->middleware('auth', ['only']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('id', 'desc')->paginate(5);
        return view('posts.index')->withPosts($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('posts.create')->withUser($user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
        $this->validate($request, array(
            'title' => 'required|max:255',
//            'slug' => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
            'body' => 'required'
        ));

        // store in the database
        $post = new Post;
        $users_id = Auth::user()->id;
        $max_post_id = Post::orderBy('id', 'desc')->first();

        $post->title = $request->title;
        $post->slug = strtolower(str_replace(' ', '_', $request->title)) . '_' . (++$max_post_id->id);
//            $request->slug;

        $post->body = $request->body;
        $users=$post->users()->associate($users_id);

        //save image
        if ($request->hasFile('featured_image')) {
            $image = $request->file('featured_image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/' . $filename);
            Image::make($image)->fit(700, 420)->save($location);

            $post->image = $filename;
        }

        $post->save();

     //   Session::flash('success', 'The blog post was successfully save!');

        return redirect()->route('blog.single', $post->slug);

        // redirect to another page
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::where('slug', $slug)->first();
        if (!$post) {
            return redirect('/')->withErrors('requested page not found');
        }
        return view('posts.show')->withPost($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.edit')->withPost($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'title' => 'required|max:255',
//                'slug' => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
            'body' => 'required'
        ));

        $max_post_id = Post::orderBy('id', 'desc')->first();
        // save the data to the database
        $post = Post::findOrFail($id);
        $post->title = $request->input('title');
        $post->slug = strtolower(str_replace(' ', '_', $request->title)) . '_' . (++$max_post_id->id);
        $post->body = $request->input('body');


        //update image
        if ($request->hasFile('featured_image')) {
            $image = $request->file('featured_image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/' . $filename);
            Image::make($image)->resize(750, 450)->save($location);
            $old_filename = $post->image;
            $post->image = $filename;
            Storage::delete($old_filename);

        }

        $post->save();

        // set flash data with success message
        Session::flash('success', 'This post was succesfully saved.');

        // redirect with flash data to posts.show
        return redirect()->route('blog.single', $post->slug);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
//        $post->tags()->detach();
        Storage::delete($post->image);

        $post->delete();

        Session::flash('success', 'The post was succesfully deleted');

        $posts = Post::orderBy('id', 'desc')->paginate(5);
        return view('posts.index')->withPosts($posts);
    }
}
