<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('perform actions and see result');

$I->amOnPage("/");

$I->click("Login");

$I->seeCurrentUrlEquals("/login");

$I->amGoingTo("create new user account");

$I->fillField("email", "user@user.com");
$I->fillField("password", "123password");

$I->click('Login');

$I->see("Latest Posts:");/*
$I->click('user');
$I->click('New post');
*/