<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('check homepage');

$I->amOnPage("/");
$I->seeInTitle("Joggin' Bloggin' | Home");
$I->see("Welcome to JB Blog Platform!","h1");

$I->see("Hello Stranger! Do you want to join in?","p");

$I->amGoingTo("check about page");

$I->click("About");
$I->seeInCurrentUrl("/about");

$I->see("About Joggin' Bloggin'","h1");
$I->see("It is a small blogging platform for people that are always on the move..","p");

$I->amGoingTo("come back to home page");
$I->click("Joggin' Bloggin'");
$I->amOnPage("/");

$I->amGoingTo("check register page");
$I->click("Register");
$I->seeInCurrentUrl("/register");
$I->see("Name");
$I->see("E-Mail Address");
$I->see("Password");
$I->see("Confirm Password");

$I->amGoingTo("come back to home page");
$I->click("Joggin' Bloggin'");
$I->amOnPage("/");


$I->amGoingTo("check login page");
$I->click("Login");
$I->seeInCurrentUrl("/login");
$I->see("E-Mail Address");
$I->see("Password");