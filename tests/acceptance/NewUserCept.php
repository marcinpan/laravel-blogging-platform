<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('perform actions and see result');

$I->amOnPage("/");

$I->click("Click!");

$I->seeCurrentUrlEquals("/register");


$I->amGoingTo("create new user account");

$I->fillField("name", "user");
$I->fillField("email", "user@user.com");
$I->fillField("password", "123password");
$I->fillField("password_confirmation", "123password");

$I->click('Register');
/*
$I->seeInDatabase("users", [
    "name" => "user"
    "email" => "user@user.com"
]);
*/