**How to setup project?**  
-1) mv joggin_bloggin/ laravel  
 0) cd laravel  
 1) composer update  
 2) cp .env.example .env  
 3) vim .env # admin|admin|admin123  
 4) php artisan key:generate  
 5) chmod -R 0777 storage  
 6) (once) delete old database on phpmyadmin  
 7) php artisan migrate
 8) php artisan db:seed
 9) if any errors occur
 * php artisan cache:clear 
 * chmod -R 0777 storage 
 * chmod -R 0777 public/images 
  
To run tests:  
1) alias codecept='./vendor/bin/codecept'  
2) codecept run