<?php $__env->startSection('title', '| Home'); ?>

<?php $__env->startSection('content'); ?>
    <?php if(!Auth::check()): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <h1>Welcome to JB Blog Platform!</h1>
                <p> Hello Stranger! Do you want to join in?</p>
                <p><a class="btn btn-primary btn-lg" href="<?php echo e(route('register')); ?>"> Click! </a></p>
            </div>
        </div>
    </div><!-- end of .row -->
    <?php endif; ?>

    <div class="row">
        <div class="col-md-7">
            <h1>Latest Posts:</h1><br><br>
            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <div class="post">
                    <?php if($post->image): ?>
                        <a href="<?php echo e(action("BlogController@getSingle", ['slug'=> $post->slug])); ?>"><img
                                    src="<?php echo e(asset('images/' . $post->image)); ?>" class="img-responsive"></a>
                    <?php endif; ?>
                    <h3>
                        <a href="<?php echo e(action("BlogController@getSingle", ['slug'=> $post->slug])); ?>"> <?php echo e($post->title); ?></a>
                    </h3>

                    <p>Author: <a href="<?php echo e(route("user.show",  $post->user_id)); ?>"><?php echo e($post->getUserName()); ?></a></p>
                    <p><?php echo e($post->created_at->diffForHumans()); ?></p>
                    <p>
                        <?php echo e(substr(strip_tags($post->body), 0, 160)); ?><?php echo strlen(strip_tags($post->body)) > 160 ? "..." : ""; ?>

                    </p>
                    <a href="<?php echo e(action("BlogController@getSingle", ['slug'=> $post->slug])); ?>">Read More</a>
                </div>
                <hr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <?php echo $posts->render(); ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-md-offset-1">
            <h1>Newest Blogs:</h1>
            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <div class="row" style="padding-top: 30px">
                    <div class="list-group-item">
                        <h3><a href="<?php echo e(route('user.show.posts', $user->id)); ?>"><?php echo e($user->name); ?>'s blog</a></h3>
                    </div>
                    <div class="list-group-item">
                        <h6>Created by <a href="<?php echo e(route('user.show', $user->id)); ?>"><?php echo e($user->name); ?></a>
                            at <?php echo e($user->created_at->format('M d, Y h:i')); ?>


                        </h6>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>