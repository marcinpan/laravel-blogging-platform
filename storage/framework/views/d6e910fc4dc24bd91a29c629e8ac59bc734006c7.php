<?php $__env->startSection('title', '| About'); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-8 col-md-offset-4">
            <h1>About Joggin' Bloggin'</h1>
            <p>
                It is a small blogging platform for people that are always on the move..
            </p>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>